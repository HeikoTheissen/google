const http = require("http");
const fs = require("fs");
const {exec} = require("child_process");
const secudir = process.env.USERPROFILE + "/Documents";
const {google} = require("googleapis");

module.exports = function(SCOPES, TOKEN_PATH) {
	TOKEN_PATH = secudir + "/" + TOKEN_PATH;
	return new Promise(function(resolve, reject) {
		authorize(JSON.parse(fs.readFileSync(secudir + "/googleapis.json")), resolve);
	});

	/**
	 * Create an OAuth2 client with the given credentials, and then execute the
	 * given callback function.
	 * @param {Object} credentials The authorization client credentials.
	 * @param {function} callback The callback to call with the authorized client.
	 */
	function authorize(credentials, callback) {
		const {client_secret, client_id, redirect_uris} = credentials.installed;
		const oAuth2Client = new google.auth.OAuth2(
			client_id, client_secret, redirect_uris[0]);
	
		// Check if we have previously stored a token.
		fs.readFile(TOKEN_PATH, (err, token) => {
			if (err) return getAccessToken(oAuth2Client, callback);
			oAuth2Client.setCredentials(JSON.parse(token));
			callback(oAuth2Client);
		});
	}
	
	/**
	 * Get and store new token after prompting for user authorization, and then
	 * execute the given callback with the authorized OAuth2 client.
	 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
	 * @param {getEventsCallback} callback The callback for the authorized client.
	 */
	function getAccessToken(oAuth2Client, callback) {
		const authUrl = oAuth2Client.generateAuthUrl({
			access_type: 'offline',
			scope: SCOPES
		});
		var loopback = http.createServer(function(req, res) {
			if (req.url === "/authorize") {
				res.statusCode = 302;
				res.setHeader("Location", authUrl);
				res.end();
			} else {
				var code = new URL("s://" + req.url).searchParams.get("code");
				if (code) {
					oAuth2Client.getToken(code, (err, token) => {
						if (err) return console.error('Error retrieving access token', err);
						oAuth2Client.setCredentials(token);
						// Store the token to disk for later program executions
						fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
							if (err) return console.error(err);
							console.log('Token stored to', TOKEN_PATH);
						});
						res.end();
						callback(oAuth2Client);
					});
					loopback.close();
				}
			}
		}).listen(80, "localhost", function() {
			exec(`explorer "http://localhost/authorize"`);
		});
	}

};
