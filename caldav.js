const https = require("https");
const authorize = require("./index");

var auth = authorize(
	["https://www.googleapis.com/auth/calendar.readonly"],
	"googlecaldavtoken.json"
);

module.exports = function(req, res, next) {
	auth.then(function(client) {
		client.getAccessToken().then(function({token}) {
			delete req.headers.host;
			req.pipe(https.request("https://apidata.googleusercontent.com" + req.url, {
				method: req.method,
				headers: Object.assign({Authorization: "Bearer " + token}, req.headers)
			}).on("response", function(r) {
				res.writeHead(r.statusCode, r.statusMessage, r.headers);
				r.pipe(res);
			}).on("error", next));
		});
	}, next);
};
