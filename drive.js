const webdav = require("webdav-server").v2;
const {PassThrough, Transform, Writable} = require("stream");
const {getType} = require("mime");
const yaml = require("js-yaml");
const {google} = require("googleapis");
const authorize = require("./index");

class Drive extends webdav.VirtualFileSystem {

	constructor(root) {
		super();
		this.drive = authorize(
			["https://www.googleapis.com/auth/drive.metadata.readonly", "https://www.googleapis.com/auth/drive.file"],
			"googledrivetoken.json"
		).then(auth => google.drive({version: "v3", auth}));
		this.resources["/"] = new webdav.VirtualFileSystemResource({
			type: webdav.ResourceType.Directory,
			props: {id: root}
		});
	}

	_fastExistCheck(ctx, path, callback) {
		var superMethod = super._fastExistCheck.bind(this);
		var dir;
		if (path.toString() in this.resources &&
				!(this.resources[path.toString()] && this.resources[path.toString()].props.new) ||
				this.resources[dir = path.getParent().toString()] && this.resources[dir].props.listed)
			superMethod(ctx, path, callback);
		else this._readDir(path.getParent(), {context: ctx}, function(err, children) {
			if (err) callback(false);
			else {
				/* this.resources[...] = undefined if we know it does not exist */
				this.resources[path.toString()] = this.resources[path.toString()];
				superMethod(ctx, path, callback);
			}
		}.bind(this));
	}

	_readDir(path, ctx, callback) {
		var superMethod = super._readDir.bind(this);
		var dir = path.toString();
		var cb = function() {
			if (this.resources[dir]) this.resources[dir].props.listed = true;
			superMethod(path, ctx, callback);
		}.bind(this);
		var ddir = dir;
		if (dir !== "/") ddir += "/";
		for (var file in this.resources)
			if (file !== "/" && file.startsWith(ddir) && file.substr(ddir.length).indexOf("/") === -1)
				delete this.resources[file];
		this._fastExistCheck(ctx, path.getParent(), function(exists) {
			if (exists && this.resources[dir]) {
				var id = this.resources[dir].props.id;
				this.drive.then(function(drive) {
					function page(that, next) {
						return drive.files.list({
							pageToken: next,
							corpora: "user",
							q: `'${id}' in parents and trashed = false`,
							fields: "files(id,name,mimeType,modifiedTime,isAppAuthorized)"
						}).then(function({data}) {
							data.files.forEach(function(file) {
								var name;
								switch (file.mimeType) {
									case "application/json":
										file.name = file.name.replace(/\.[^.]*$/, ".yml");
										break;
								}
								if (dir === "/") name = dir + file.name;
								else name = dir + "/" + file.name;
								if (file.mimeType === "application/vnd.google-apps.folder")
									that.resources[name] = new webdav.VirtualFileSystemResource({
										type: webdav.ResourceType.Directory,
										props: {id: file.id}
									});
								else if (file.isAppAuthorized)
									that.resources[name] = new webdav.VirtualFileSystemResource(Object.assign({
										type: webdav.ResourceType.File,
										size: -1,
										lastModifiedDate: new Date(file.modifiedTime),
										props: {id: file.id}
									}, file));
							});
							if (data.nextPageToken) page(that, data.nextPageToken);
							else cb();
						});
					}
					return page(this);
				}.bind(this), function(err) {
					callback(webdav.Errors.Forbidden);
				});
			} else cb();
		}.bind(this));
	}

	_openReadStream(path, ctx, callback) {
		var file = this.resources[path.toString()];
		return this.drive.then(function(drive) {
			console.log(path.toString(), file.props.id);
			var type = getType(path.fileName());
			var request = drive.files.get({
				fileId: file.props.id,
				alt: "media"
			}, {responseType: type === "text/yaml" ? "json" : "stream"});
			request.then(function({data}) {
				if (type === "text/yaml") {
					var readable = new PassThrough();
					readable.end(yaml.safeDump(data));
					return readable;
				} else
					return data;
			}).then(callback.bind(undefined, null), function(err) {
				callback(webdav.Errors.Forbidden);
			});
			return request;
		});
	}

	_openWriteStream(path, ctx, callback) {
		var file = this.resources[path.toString()];
		return this.drive.then(function(drive) {
			var buffers = [];
			var media = {mimeType: getType(path.fileName())};
			if (media.mimeType === "text/yaml") {
				media.mimeType = "application/json";
				media.body = new Transform({
					transform(chunk, encoding, callback) {
						buffers.push(chunk);
						callback();
					},
					flush(callback) {
						callback(null, JSON.stringify(yaml.safeLoad(Buffer.concat(buffers).toString())));
					}
				});
			} else
				media.body = new PassThrough();
			var request;
			if (file.props.id)
				request = drive.files.update({
					fileId: file.props.id,
					media: media
				});
			else {
				var body = {name: path.fileName()};
				var dir = this.resources[path.getParent().toString()];
				if (dir) body.parents = [dir.props.id];
				request = drive.files.create({
					requestBody: body,
					media: media
				});
				request.then(function({data}) {
					console.log(path.toString(), data.id);
				});
				this.resources[path.toString()] = new webdav.VirtualFileSystemResource({
					type: webdav.ResourceType.File,
					size: -1,
					props: {new: true}
				});
			}
			callback(null, new Writable({
				write(chunk, encoding, callback) {
					media.body.write(chunk, encoding, callback);
				},
				final(callback) {
					media.body.end();
					request.then(function() {
						callback();
					}, this.destroy.bind(this));
				}
			}));
			return request;
		}.bind(this));
	}

}

if (module.parent)
	module.exports = Drive;
else {
	var server = new webdav.WebDAVServer({
		hostname: "localhost",
		httpAuthentication: {
			askForAuthentication() {
				return [];
			},
			getUser(ctx, callback) {
				callback(null, {uid: "Default", username: "Default"});
			}
		}
	});
	server.setFileSystemSync("/", new Drive("root"));
	server.start(function(httpServer) {
		httpServer.on("request", function(req, res) {
			res.on("finish", function() {
				console.log(req.method, decodeURI(req.url), res.statusCode, res.statusMessage);
			});
		});
		console.log("WebDAV server running", httpServer.address());
	});
}
