const {google} = require("googleapis");
const authorize = require("./index");

var auth = authorize(
	["https://www.googleapis.com/auth/gmail.readonly"],
	"googlemailtoken.json"
).then(auth => google.gmail({version: "v1", auth}));

module.exports = function(req, res, next) {
	auth.then(mail => mail.users.messages.list({
		userId: "me",
		q: "is:unread has:attachment"
	}).then(function({data}) {
		if (!data.messages) throw "No unread mail with attachments";
		return Promise.all(data.messages.map(message => new Promise(function(resolve, reject) {
			mail.users.messages.get({
				userId: "me",
				id: message.id,
				fields: "payload.parts"
			}).then(function({data}) {
				var attach = data.payload.parts.find(_ => _.filename === req.params.filename);
				resolve(attach && {
					messageId: message.id,
					attach
				});
			});
		}))).then(function(attachs) {
			for (var a of attachs) if (a)
				return mail.users.messages.attachments.get({
					userId: "me",
					messageId: a.messageId,
					id: a.attach.body.attachmentId,
					fields: "data"
				}).then(function({data}) {
					res.type(a.attach.mimeType);
					res.end(Buffer.from(data.data, "base64"));
				});
			throw "No such attachment";
		});
	}).catch(next));
};
